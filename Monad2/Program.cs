﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Sprache;

namespace Monad2
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0 || !File.Exists(args[0]))
            {
                Console.WriteLine("Файл не найден");
                return;
            }

            try
            {
                string input = File.ReadAllText(args[0]);
                var res = S.Parse(input);
                Console.WriteLine("S->");
                res.TreeOutputToConsole();
                Console.ReadKey();
            }
            catch (Exception exception)
            {
                Console.WriteLine("ERROR");
                Console.WriteLine(exception.Message);
            }

        }

        private const int CommandInBlockMax = 10;

        #region SimpleCommand
        public static Parser<Command> SplitFunc =
            from x in Parse.Char(';').AtLeastOnce()
            select new Command() { Type = Command.CommandType.Split };


        public static Parser<Command> NewLineFunc =
            from x in Parse.Char('\n').AtLeastOnce()
            select new Command() { Type = Command.CommandType.NewLine };

        public static Parser<Command> SkipFunc =
            from x in Parse.String("skip").Token().Text()
            select new Command() { Type = Command.CommandType.Skip };

        public static Parser<Command> WriteFunc =
        from x in Parse.String("write").Token().Text()
        from operand in Variable.Token()
        select new Command() { Type = Command.CommandType.Write, View = operand.View };

        public static Parser<Command> ReadFunc =
            from x in Parse.String("read").Token().Text()
            from operand in Variable
            select new Command() { Type = Command.CommandType.Read, View = operand.View };




        #endregion


        #region Variable
        private static readonly string[] _operationsStrings = new[]
       {
            ">",
            "<",
            "<=",
            ">=",
            "+",
            "-",
            "*",
            "/",
            "==",
            "!=",
            "&&",
            "||",
        };

        private static readonly string[] KeyWords = new[] { "read", "then", "skip", "do", "else", "od", "if", "fi", "split" };
        private static readonly char[] OperationChars = new[] { '>', '<', '=', '+', '*', '|', '-', '!', '&', '/' };

        private static bool VariableIsValid(string text)
        {
            var op = text.Trim();
            if (!string.IsNullOrEmpty(op) && KeyWords.Contains(op))
                throw new ParseException("Неверное название переменной");

            return true;
        }



        private static readonly Parser<string> VariableSymb =
              (
            from letterPartVar in Parse.Letter.AtLeastOnce().Text()
            from digitPartVar in Parse.Digit.Many().Text()
            let result = letterPartVar + digitPartVar
            where VariableIsValid(result)
            select result).Text();

        private static readonly Parser<Command> VariableAsExpressionWithoutParentheses = VariableSymb.Select(x => new Command() { Type = Command.CommandType.Variable, View = x });

        public static Parser<Command> VariableAsExpressionInParentheses = from lparen in Parse.Char('(')
                                                                          from expr in (VariableAsExpressionWithoutParentheses).Token()
                                                                          from rparen in Parse.Char(')')
                                                                          select expr;
        private static Parser<Command> Variable
            => VariableAsExpressionInParentheses.XOr(VariableAsExpressionWithoutParentheses);

        #endregion
        private static bool OperationIsValid(string operation)
        {
            var op = operation.Trim();
            if (!string.IsNullOrEmpty(op) && !_operationsStrings.Contains(operation))
                throw new Exception("Операции не существует");
            return true;
        }

        public static Parser<Command> Operations =
            from operation in Parse.Chars(OperationChars).AtLeastOnce().Text()
            where OperationIsValid(operation)
            select new Command() { Type = Command.CommandType.Operation, View = operation };


        public static Parser<Command> Empty = from o in Parse.Return(Empty)
                                              select new Command() { Type = Command.CommandType.Empty };


        public static Parser<Command> Else = from o in Parse.String("else").Token()
                                             select new Command() { Type = Command.CommandType.Else };


        private static readonly Parser<Command> ExpressionInParentheses = from lparen in Parse.Char('(')
                                                                          from expr in (ExpressionWithoutParentheses).Token()
                                                                          from rparen in Parse.Char(')')
                                                                          select expr;

        private static readonly Parser<Command> Numbers = from num in Parse.Digit.Many().Text().Token() select new Command() { Type = Command.CommandType.Number, View = num };

        private static readonly Parser<Command> ExpressionWithoutParentheses
            = from operand1 in Variable.XOr(Numbers).Token()
              from operation in Operations.Or(Empty).Token()
              from operand2 in !string.IsNullOrEmpty(operation.View) ? Variable.XOr(Numbers) : Empty
              select new Command()
              {
                  Type = Command.CommandType.Expression,
                  ChildCommands = new List<Command>() { operand1, operation, operand2 }
              };

        public static Parser<Command> Expression => ExpressionInParentheses.XOr(ExpressionWithoutParentheses).XOr(Variable).XOr(Numbers);

        public static Parser<string> NotKeywords = Parse.String("fi").Text();


        public static Parser<IEnumerable<Command>> IfConfitionElse =>
          from operations in P.Repeat(0, CommandInBlockMax)
          select (new List<Command>() { new Command() { Type = Command.CommandType.Else } }).Concat(operations);

        public static Parser<Command> IfConfition =
           (from ifBegin in Parse.String("if").Token().Text()
            from exp in Expression.Token()
            from thenCond in Parse.String("then").Token().Text()
            from operations in P.Until(Parse.String("fi")).Repeat(0, CommandInBlockMax)
                // from elseCom in Parse.IgnoreCase("else").Then(x => IfConfitionElse).Or(Empty.Select(x => new List<Command>()))

            select new Command() { Type = Command.CommandType.If, ChildCommands = (new List<Command>() { exp }).Concat(operations.ElementAt(0)).ToList() });



        public static Parser<Command> While =
              (from ifBegin in Parse.String("while").Token().Text()
               from exp in Expression.Token()
               from thenCond in Parse.String("do").Token().Text()
               from operations in P.Until(Parse.String("od")).Repeat(0, CommandInBlockMax)

               select new Command() { Type = Command.CommandType.While, ChildCommands = (new List<Command>() { exp }).Concat(operations.ElementAt(0)).ToList() });



        public static Parser<Command> AssigmentFunc =

          from var in Variable
          from x in Parse.String(":=").Token().Text()
          from operand in Expression
          select new Command() { Type = Command.CommandType.Assigment, ChildCommands = operand.ChildCommands };



        private static Parser<Command> P
            =>
                (While).Or(WriteFunc)
                    .Or(ReadFunc)
                    .Or(SplitFunc)
                    .Or(NewLineFunc)
                    .Or(SkipFunc)
                    .Or(IfConfition)
                    .Or(Else)
            .Or(AssigmentFunc);



        private static Parser<IEnumerable<Command>> S => P.Repeat(0, 16);



    }
}
