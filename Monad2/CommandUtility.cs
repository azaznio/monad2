﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monad2
{
    public static class CommandUtility
    {
        public static void TreeOutputToConsole(this IEnumerable<Command> commands)
        {
            OutputTree(commands);
        }
        private static void OutputTree(IEnumerable<Command> commands, int deep = 1)
        {
            foreach (var command in commands)
            {
                SpacesToConsole(deep);
                Console.Write(CommandToStringForTree(command, deep) + "\n\n");
                /* if (command.ChildCommands != null && command.ChildCommands.Any())
                     OutputTree(command.ChildCommands, deep + 2);*/
            }
        }

        private static string CommandToStringForTree(Command command, int deep)
        {
            switch (command.Type)
            {
                case Command.CommandType.Assigment:
                    string output = "Assigment->\t" + GetVarOutput(command.ChildCommands.ToList()[0].View);

                    if (!string.IsNullOrWhiteSpace(command.ChildCommands.ToList()[1].View))
                    {
                        output += "\n" + Spaces(deep + 6) + "Operation->"+(command.ChildCommands.ToList()[1].View);
                        output += "\n" + Spaces(deep + 6) + GetVarOutput(command.ChildCommands.ToList()[2].View);
                    }

                    return output;
                case Command.CommandType.NewLine:
                    return "NewLine->eps";
                case Command.CommandType.Split:
                    return "Split->eps";
                case Command.CommandType.Skip:
                    return "Skip->eps";
                case Command.CommandType.Read:
                    return "Read->" + GetVarOutput(command.View);
                case Command.CommandType.Write:
                    return "Write->" + GetVarOutput(command.View);

                case Command.CommandType.If:
                    {
                        var condition = "Condition->Expression->\t" +
                                        GetVarOutput(command.ChildCommands.ToList()[0].ChildCommands.ToList()[0].View);

                        if (!string.IsNullOrWhiteSpace(command.ChildCommands.ToList()[0].ChildCommands.ToList()[1].View))
                            condition += "\n" + Spaces(deep + 4) + "Operation-> " + (command.ChildCommands.ToList()[0].ChildCommands.ToList()[1].View) +
                                            "\n" + Spaces(deep + 4) + GetVarOutput(command.ChildCommands.ToList()[0].ChildCommands.ToList()[2].View);

                        string Ifthen = "\n" + Spaces(deep + 1) + "->Then->";
                        if (command.ChildCommands.Count > 1)
                            for (int i = 1; i < command.ChildCommands.Count; i++)
                            {
                                if (command.ChildCommands.ToList()[i].Type == Command.CommandType.Else)
                                    Ifthen += "\n" + Spaces(deep + 1) + "->Else->";
                                else
                                    Ifthen += "\n" + Spaces(deep + 4) + CommandToStringForTree(command.ChildCommands.ToList()[i], deep);
                            }

                        return "If      ->" + condition + "\n" + Spaces(deep + 4) + Ifthen;
                    }
                case Command.CommandType.While:
                    {
                        var condition = "Condition->Expression->\t" +
                                        GetVarOutput(command.ChildCommands.ToList()[0].ChildCommands.ToList()[0].View);

                        if (!string.IsNullOrWhiteSpace(command.ChildCommands.ToList()[0].ChildCommands.ToList()[1].View))
                            condition += "\n" + Spaces(deep + 4) + "Operation-> " + (command.ChildCommands.ToList()[0].ChildCommands.ToList()[1].View) +
                                            "\n" + Spaces(deep + 4) + GetVarOutput(command.ChildCommands.ToList()[0].ChildCommands.ToList()[2].View);

                        string Ifthen = string.Empty;
                        if (command.ChildCommands.Count > 1)
                            for (int i = 1; i < command.ChildCommands.Count; i++)
                            {
                                Ifthen += "\n" + Spaces(deep + 4) + CommandToStringForTree(command.ChildCommands.ToList()[i], deep);
                            }

                        return "While      ->" + condition + "\n" + Spaces(deep + 4) + Ifthen;
                    }


            }
            return string.Empty;
        }


        private static void SpacesToConsole(int deep) => Enumerable.Range(0, deep).ToList().ForEach(x => Console.Write("\t"));
        private static string Spaces(int deep)
        {
            string output = String.Empty;
            Enumerable.Range(0, deep).ToList().ForEach(x => output += "\t");
            return output;
        }
        public static string GetVarOutput(string text)
        {
            try
            {
                Int32.Parse(text);
                return "Var->Number->" + text;
            }
            catch { }

            return "Var->" + text;
        }

    }
}
