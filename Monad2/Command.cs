﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monad2
{
   public class Command
    {
        public CommandType Type { get; set; }
        public ICollection<Command> ChildCommands { get; set; } 
        public string View { get; set; }

       public override string ToString()
       {
           return View.ToString();
       }

       public enum CommandType
        {
            Variable,
            Read,
            Write,
            Skip,
            Split,
            Assigment,
            If,
            While,
            NewLine,
            Operation,
            Number,
            Empty,
            Expression,
            Fi,
            Od,
            Else
        }
    }
}
